#
# Be sure to run `pod lib lint SSGridView.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "SSGridView"
  s.version          = "0.0.1"
  s.summary          = "A brief description of SSGridView."
  s.description      = "A large and usefull description of SSGridView."
  s.homepage         = "https://ikalyuzhniy@bitbucket.org/ikalyuzhniy/ssgridview.git"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Ilya Kalyuzhniy" => "ilya.kalyuzhniy@dataart.com" }
  s.source           = { :git => "https://ikalyuzhniy@bitbucket.org/ikalyuzhniy/ssgridview.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'SSGridView' => ['Pod/Assets/*.png']
  }
  s.dependency 'SDWebImage', '~> 3.7.1'
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
